﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour {

    [SerializeField]
    protected Graph graph;

    [SerializeField]
    protected Node start;

    [SerializeField]
    protected Node end;

    [SerializeField]
    protected float speed = 0.01f;

    protected Path path = new Path();
    protected Node current;

    private void Start()
    {
        path = graph.GetShortestPath(start, end);
        FollowPath(path);
    }

    public void FollowPath(Path path)
    {
        StopCoroutine(MoveTowardPath());
        this.path = path;
        transform.position = path.getNodes[0].transform.position;
        StartCoroutine(MoveTowardPath());
    }

    IEnumerator MoveTowardPath()
    {
#if  UNITY_EDITOR
        UnityEditor.EditorApplication.update += Update;
#endif

        var e = path.getNodes.GetEnumerator();
        while (e.MoveNext())
        {
            current = e.Current;

            yield return new WaitUntil(() =>
            {
                return transform.position == current.transform.position;
            });

        }

        current = null;

#if UNITY_EDITOR
        UnityEditor.EditorApplication.update -= Update;
#endif
    }

    private void Update()
    {
        if (current != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, current.transform.position, speed);
        }
    }
}
