﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PathFinding : MonoBehaviour {

    public Grid grid;
    public Transform startpos;
    public Transform targetPos;

    private void Awake()
    {
        grid = GetComponent<Grid>();
    }

    void LateUpdate()
    {
        FindPath(startpos.position, targetPos.position);
        if(grid.FinalPath != null)
        {
            Node final;
            final = grid.FinalPath[grid.FinalPath.Count - 1];
            if(final == grid.NodeFromWorldPoint(targetPos.position))
            {
                grid.canMove = true;
            }
            else
            {
                grid.canMove = false;
            }
        }
    }

    void FindPath(Vector3 start, Vector3 target)
    {
        Node startNode = grid.NodeFromWorldPoint(start);
        Node targetNode = grid.NodeFromWorldPoint(target);

        List<Node> openNodes = new List<Node>();
        HashSet<Node> closedNodes = new HashSet<Node>();

        openNodes.Add(startNode);

        while (openNodes.Count > 0)
        {
            Node currentNode = openNodes[0];

            for (int i = 1; i < openNodes.Count; i++)
            {
                if (openNodes[i].F_Cost < currentNode.F_Cost || openNodes[i].F_Cost == currentNode.F_Cost && openNodes[i].H_Cost < currentNode.H_Cost)
                {
                    currentNode = openNodes[i];
                }
            }

                openNodes.Remove(currentNode);
                closedNodes.Add(currentNode);

                if (currentNode == targetNode)
                {
                    GetFinalPath(startNode, targetNode);
                }

                foreach(Node neighbor in grid.GetNeighboringNode(currentNode))
                {
                    if (!neighbor.IsWall || closedNodes.Contains(neighbor))
                    {
                        continue;
                    }

                    int moveCost = currentNode.G_Cost;
                    GetManhattanDistance(currentNode, neighbor);

                    if(moveCost < neighbor.G_Cost || !openNodes.Contains(neighbor))
                    {
                        neighbor.G_Cost = moveCost;
                        neighbor.H_Cost = GetManhattanDistance(neighbor, targetNode);
                        neighbor.ParentNode = currentNode;

                        if (!openNodes.Contains(neighbor))
                        {
                            openNodes.Add(neighbor);
                        }
                    }
                }
            
        }
    }

    void GetFinalPath(Node start, Node end)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = end;

        while (currentNode != start)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.ParentNode;
        }

        finalPath.Reverse();
        grid.FinalPath = finalPath;
    }

    int GetManhattanDistance(Node nodeA, Node nodeB)
    {
        int x = Mathf.Abs(nodeA.GridPosX - nodeB.GridPosX);
        int y = Mathf.Abs(nodeA.GridPosY - nodeB.GridPosY);
        return x + y;
    }
}
