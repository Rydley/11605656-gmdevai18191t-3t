﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

    public bool canMove;

    public Transform StartPosition;

    public LayerMask WallMask;

    public Vector2 GridWorldSize = new Vector2(30, 30);

    public float NodeRadius = 0.5f;

    public float DistanceBetweenNodes = 0;

    public Node[,] NodeArray;

    public List<Node> FinalPath;

    public float NodeDiameter;

    public int GridSizeX, GridSizeY;

    private void Start()
    {
        NodeDiameter = NodeRadius * 2;
        GridSizeX = Mathf.RoundToInt(GridWorldSize.x / NodeDiameter);
        GridSizeY = Mathf.RoundToInt(GridWorldSize.y / NodeDiameter);
        NodeArray = new Node[GridSizeX, GridSizeY];
    }

    private void Update()
    {
        CreateGrid();
    }

    private void CreateGrid()
    {
        
        Vector3 bottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.forward * GridWorldSize.y / 2;

        for(int x = 0; x < GridSizeX; x++)
        {
            for (int y = 0; y < GridSizeY; y++)
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * NodeDiameter + NodeRadius) + Vector3.forward * (y * NodeDiameter + NodeRadius);

                bool isWall = true;

                if (Physics.CheckSphere(worldPoint, NodeRadius, WallMask))
                {
                    isWall = false;
                }

                NodeArray[x, y] = new Node(isWall, worldPoint, x, y);
            }
        }
    }

    public List<Node> GetNeighboringNode(Node neighborNode)
    {
        List<Node> neighborList = new List<Node>();
        int checkX, checkY;

        checkX = neighborNode.GridPosX + 1;
        checkY = neighborNode.GridPosY;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        checkX = neighborNode.GridPosX - 1;
        checkY = neighborNode.GridPosY;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        checkX = neighborNode.GridPosX;
        checkY = neighborNode.GridPosY + 1;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        checkX = neighborNode.GridPosX;
        checkY = neighborNode.GridPosY - 1;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        return neighborList;
    }

    public Node NodeFromWorldPoint(Vector3 worldPos)
    {
        float xPos = ((worldPos.x + GridWorldSize.x / 2) / GridWorldSize.x);
        float yPos = ((worldPos.z + GridWorldSize.y / 2) / GridWorldSize.y);

        xPos = Mathf.Clamp01(xPos);
        yPos = Mathf.Clamp01(yPos);

        int x = Mathf.RoundToInt((GridSizeX - 1) * xPos);
        int y = Mathf.RoundToInt((GridSizeY - 1) * yPos);

        return NodeArray[x, y];
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, 1, GridWorldSize.y));

        if (NodeArray != null)
        {
            foreach (Node n in NodeArray)
            {
                if (n.IsWall)
                {
                    Gizmos.color = Color.white;
                }

                else
                {
                    Gizmos.color = Color.yellow;
                }

                if (FinalPath != null)
                {
                    if (FinalPath.Contains(n))
                    {
                        Gizmos.color = Color.red;
                    }
                }
                Gizmos.DrawCube(n.Position, Vector3.one * (NodeDiameter - DistanceBetweenNodes));
            }
        }
    }
}
