﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PathFollower : MonoBehaviour {

    public Grid grid;

    public float maxSpeed;
    public float currentSpeed;

    private List<Node> path;
    private Node current;

    private void Start()
    {
        currentSpeed = maxSpeed;
    }
	
	private void Update ()
    {
        if (grid.canMove)
        {
            currentSpeed = maxSpeed;
        }
        else
        {
            currentSpeed = 0;
        }
        
        if (path != null && current != path[path.Count - 1])
        {
            if (current == null)
            {
                current = path[0];
                transform.position = current.Position;
            }
            else if (path.Count() > 1)
            {
                current = path[1];
                transform.position = Vector3.MoveTowards(transform.position, current.Position, currentSpeed);
            }
        }
        
	}

    private void LateUpdate()
    {
        path = grid.FinalPath;
    }
}
