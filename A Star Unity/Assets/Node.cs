﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    public int GridPosX;
    public int GridPosY;

    public bool IsWall;

    public Vector3 Position;

    public Node ParentNode;

    public int G_Cost;
    public int H_Cost;

    public int F_Cost
    {
        get
        {
            return G_Cost + H_Cost;
        }
    }

    public Node(bool isWall, Vector3 worldPos, int gridX, int gridY)
    {
        this.IsWall = isWall;
        this.Position = worldPos;
        this.GridPosX = gridX;
        this.GridPosY = gridY;
    }
}
