﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Escape")]
public class EscapeAction : Action {

    public override void Act(StateController controller)
    {
        Escape(controller);
    }

    private void Escape(StateController controller)
    {
        controller.navMeshAgent.destination = -controller.chaseTarget.position;
        controller.navMeshAgent.Resume();
    }
}
