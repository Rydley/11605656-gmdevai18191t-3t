﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/EscapeState")]
public class EscapeDecision : Decision
{
    
    public override bool Decide(StateController controller)
    {
        bool IsHealthLow = HealthCheck(controller);
        return IsHealthLow;
    }

    private bool HealthCheck(StateController controller)
    {
        if(controller.GetComponent<TankHealth>().m_CurrentHealth <= 50.0f)
        {
            return true;
        }

        else
        {
            return false;
        }
    }
}
